//Se muestra la temperatura y la presion respecto a la altura, pero de manera independiente
//Se ejecuta en Processing 3.5.4

import ddf.minim.*; // Libreria para poder usar sonidos
                    // Descargar el menú: Sketch>Import Library...>Add Library...>
                    // Buscar "Minim", seleccionar "Minim" e Intalar
Minim minim;
AudioPlayer alarma;

int ColorTemperatura=0;     //Variable de color, max 255
int ColorPresion=0;         //Variable de color, max 255
int TemperaturaInicial=20;  //Temperatura inicial nivel del mar
int PresionInicial=100;     //Presion inicial nivel del mar
int Temperatura;            //Var. de temperatura
int Presion;                //Var. de Presión

int AlturaT;                //Var. para la barra deslizante de altura de temp.
int AlturaP;                //Var. para la barra deslizante de altura de presión


void setup() {
  delay(50);
  background(240);            //Color de fondo en escala de grises
  //size(590, 230);           //Tamaño de la ventana
  minim = new Minim(this);
  alarma = minim.loadFile("1000Hz.wav");  //Llamado del archivo de sonido de alarma
  alarma.loop();
  alarma.mute();
}

public void settings() {
  size(590, 230);
}

void draw() {
  background(240);          //útil para evitar la superposicion de texto (overlapping)
  
                            //Forma del punto de prueba de temperatura
  fill(100,0,ColorTemperatura);
  rect(40,50, 150, 150);
  textSize(15);
  fill(0);
  text("Sensor de temperatura",40,20);
  
                            //Forma del punto de prueba de presión
  fill(100,ColorPresion,0);
  rect(400,50,150,150);
  textSize(15);
  fill(0);
  text("Sensor de Presión",400,20);
    
                            //Muestra de valores numéricos de temp. y presión
  text("T= ",40,40);
  Temperatura=int(TemperaturaInicial-ColorTemperatura*0.24);
  text(Temperatura,60,40);
  text(" ºC",80,40);
  delay(10);
  text("P= ",400,40);
  Presion=int(PresionInicial-ColorPresion*0.28);
  text(Presion,420,40);
  text(" kPa",450,40);
  
                             //Pie de ventana
  text("Temperatura y Presión respecto a la altura", 160, 220);
  
  
                             //Altura que muestra la barra deslizante
  AlturaT=int(ColorTemperatura*0.04);
  text(AlturaT+"km",210,200-ColorTemperatura*0.549);
  
  AlturaP=int(ColorPresion*0.04);
  text(AlturaP+"km",355,200-ColorPresion*0.549);

  
                             //Mouse sobre el sensor de Temperatura
  if(pmouseX<=190 & pmouseX>=40 & pmouseY>=50 & pmouseY<=200){
    ColorTemperatura=ColorTemperatura+2; //La temperatura del color aumenta en 2 su valor por cada ciclo
    if(ColorTemperatura>=255){           //El máximo de color es 255 (por la escala RGB)
    ColorTemperatura=255;
    }
  }
  else{
    ColorTemperatura=ColorTemperatura-2; //Cambio del color hacia el valor inicial, -2 en cada ciclo
    if(ColorTemperatura<=0){             //El mínimo de color es 0 (por la escala RGB)
      ColorTemperatura=0;
    }
  }
  
                                         //Mouse sobre el sensor de Presión (igual que en la temperatura pero con valor inicial diferente)
  if(pmouseX<=550 & pmouseX>=400 & pmouseY>=50 & pmouseY<=200){
    ColorPresion=ColorPresion+2;
    if(ColorPresion>=255){
    ColorPresion=255;
    }
  }
  else{
    ColorPresion=ColorPresion-2;
    if(ColorPresion<=0){
    ColorPresion=0;
    }
  }
  
    
                    //Activación/desactivación de la alarma sonora al llegar al minimo de los valores (segun la altura)
  if(Temperatura<-40 || Presion<30){
    alarma.unmute();
  }
  else {    
    alarma.mute();     
  }
  
                    //Alarma Visual, cambia de color la zona del sensor
  if(Temperatura<-40){  
    fill(255,233,0);
    rect(40,50,150,150);
    }
  if(Presion<30){  
    fill(255,233,0);
    rect(400,50,150,150);
  }


                //Barra deslizante que muestra la altura segun el color de la zona del sensor
                //Ambos sensores son independientes
  fill(100);
  rect(190,50,10,150);
  fill(255);
  rect(190,190-ColorTemperatura*0.549,10,10);
  
  fill(100);
  rect(390,50,10,150);
  fill(255);
  rect(390,190-ColorPresion*0.549,10,10); 
  
}

  
    

 
